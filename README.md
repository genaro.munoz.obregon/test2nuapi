# API for Nu image medical Test

## Git clone

```
git clone https://gitlab.com/genaro.munoz.obregon/test2nuapi.git test2nuapi
```

## Ingresar a la carpeta apinumedicaltest

```
cd test2nuapi
```

## Instalar vendor

```
composer install
```

## Configurar archivo .env con la conexion a la base de datos

Ejemplo:

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=testnuimage
DB_USERNAME=root
DB_PASSWORD=yourpassword

```

## Correr migraciones

Ejemplo:

```
php artisan migrate --seed

```

## Correr server local (Con el fin de evitar problema de CORS localmente)

Ejemplo:

```
php artisan serve

```

En este caso es posible que corra en la siguiente url http://127.0.0.1:8000 la cual deberia configurar en el archivo .env del proyecto front
