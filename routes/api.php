<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\v1\RegisterController as RegisterV1;
use App\Http\Controllers\Api\v1\VINController as VINV1;
use App\Http\Controllers\Api\v1\OTPController as OtpV1;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::post('login', [App\Http\Controllers\Api\LoginController::class, 'login']);


Route::prefix('v1/user')->group(function () {
    Route::post('/register', [RegisterV1::class,'store']);
});

Route::prefix('v1/otp')->group(function () {
    Route::post('/generate', [OtpV1::class, 'generate_otp']);
    Route::post('/validate', [OtpV1::class, 'validate_otp']);
});

Route::group([
    'middleware' => 'auth:api'
],function () {

  
    Route::get('v1/user', [RegisterV1::class,'index']);
   

    Route::prefix('v1/search')->group(function () {
        Route::post('/decode', [VINV1::class, 'get_Decode_VIN']);
        Route::post('/salvage', [VINV1::class, 'get_SalvageCheck_VIN']);
    });
});
