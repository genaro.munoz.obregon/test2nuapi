<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\Models\User;

class LoginController extends Controller
{
    public function login(Request $request){
        $this->validateLogin($request);             
        if(Auth::attempt(['username' => $request->username, 'password' => $request->password])){            
            return response()->json([
                'token' => $request->user()->createToken('Logueado'.time().'User'.$request->username)->plainTextToken,
                'message' => 'Success',
                'status' => 'Success',
            ]);
        }else{
            return response()->json([
                'token' => '',
                'message' => 'Not match found',
                'status' => 'Error',
            ]);
        }

        /*return response()->json([
            'message'=>'Unauthenticated',
            'status' => 'User can not connect',
        ], 401);*/

    }

    public function validateLogin(Request $request)
    {
        return $request->validate([
            'username'=>'required',
            'password'=>'required'
        ]);
    }
}
