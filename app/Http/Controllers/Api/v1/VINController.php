<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Traits\RapidApi;

class VINController extends Controller
{
    use RapidApi;

    public function get_SalvageCheck_VIN(Request $request){
      
            $validateVIN = $this->ValidateVIN_SalvageCheck($request->vin);

            return response([
                'status'=>'Success',
                'searchVIN'=>$validateVIN,
            ],200);
       
    }

    public function get_Decode_VIN(Request $request){
        
            $validateVIN = $this->ValidateDecodeVIN($request->vin);
            
            return response([
                'status' => 'Success',
                'searchVIN'=>$validateVIN,
            ],200);

       
    }
}
