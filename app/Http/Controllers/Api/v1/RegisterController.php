<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Resources\UserResource;
use App\Http\Requests\RegisterRequest;
use App\Http\Traits\RapidApi;

class RegisterController extends Controller
{

    use RapidApi;

    public function index(Request $request){

        
        try{
            return response([
                'message'=>'Success',
                'users'=>new UserResource(User::all()),
            ],200);
            /*return response()->json([
                'name' => 'Abigail',
                'state' => 'CA',
            ]);*/
        }catch(\Throwable $e){
            return response([
                'message'=>$e->getMessage(),
            ],500);
        }
    }

    public function store(RegisterRequest $request){
        try{
            $register = User::create([
                'name'=>$request->name,
                'username'=>$request->username,
                'phone'=>$request->phone,
                'email'=>$request->email,
                'password'=>bcrypt($request->password)
            ]);

            //$validatePhone = $this->ValidateRegisterPhoneNumber($request->phone,'57');
            return response([
                'status' => 'Success',
                'title' => 'Your registration was successful',
                'message'=>'The form was submitted and saved!',
                'user'=>new UserResource($register),
            ]);
        }catch(\Throwable $e){
            return response([
                'status' => 'Error',
                'error'=>$e->getMessage(),
            ],500);
        }
        
    }

}
