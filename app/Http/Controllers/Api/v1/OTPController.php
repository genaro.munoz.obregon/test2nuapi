<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Traits\RapidApi;

class OTPController extends Controller
{
    public function generate_otp(Request $request){

        $curl = curl_init();

        $phonenumber = "+".$request->codeco.$request->phonenumber;

        curl_setopt_array($curl, [
            CURLOPT_URL => "https://d7sms.p.rapidapi.com/verify/v1/otp/send-otp",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode([
                'originator' => 'SignOTP',
                'recipient' => $phonenumber,
                'content' => 'Thanks for register, your mobile verification code is: {}',
                'expiry' => '600',
                'data_coding' => 'text'
            ]),
            CURLOPT_HTTPHEADER => [
                "Token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJhdXRoLWJhY2tlbmQ6YXBwIiwic3ViIjoiNDBiNzRiMzUtNDU1My00Y2MzLWFjNGYtOWQyZTA0OWVlYzNjIn0.Te3e9gJhRQku-fHEj4nMH8lvnleY2uzewlhIjJeLxZ8",
                "X-RapidAPI-Host: d7sms.p.rapidapi.com",
                "X-RapidAPI-Key: 7b49280972mshd98e8d8eaaf647bp16bffejsn7d48cb949ce7",
                "content-type: application/json"
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return [
                'success' => 200,
                'status' => 'Success',
                'data'    => json_decode($response,true),
            ];
        }

    }

    public function validate_otp(Request $request){

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => "https://d7sms.p.rapidapi.com/verify/v1/otp/verify-otp",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode([
                'otp_id' => $request->otp,
		        'otp_code' => $request->code
            ]),
            CURLOPT_HTTPHEADER => [
                "Token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJhdXRoLWJhY2tlbmQ6YXBwIiwic3ViIjoiNDBiNzRiMzUtNDU1My00Y2MzLWFjNGYtOWQyZTA0OWVlYzNjIn0.Te3e9gJhRQku-fHEj4nMH8lvnleY2uzewlhIjJeLxZ8",
                "X-RapidAPI-Host: d7sms.p.rapidapi.com",
                "X-RapidAPI-Key: 7b49280972mshd98e8d8eaaf647bp16bffejsn7d48cb949ce7",
                "content-type: application/json"
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return [
                'success' => 200,
                'status' => 'Success',
                'data'    => json_decode($response,true),
            ];
        }

    }
}
