<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
       
            return [
                'name'=>'required',
                'username'=> 'required|unique',
                'phone' => 'required',
                'email' => 'required|email',
                'password' => 'required|min:6'
            ];
        
    }

    public function message(){
        return [
            'name.required'=>'El campo nombre es requerido',
            'username.required'=>'El campo username es requerido',
            'username.required'=>'El campo username debe ser unico',
            'phone.required'=>'El campo Teléfono es requerido',
            'email.required'=> 'Email es requerido',            
            'email.email' => 'Email no tiene formato valido',
            'password.required'=>'Password es requerido',
            'password.min'=>'El minimo de caracteres para la contraseña es de 6'
        ];
    }
}
