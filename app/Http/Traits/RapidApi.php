<?php
namespace App\Http\Traits;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;
use Throwable;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;


use GuzzleHttp\Client;

/**
 * trait RapidApi
 */
trait RapidApi
{
    public $RapidAPIhost = 'telesign-telesign-send-sms-verification-code-v1.p.rapidapi.com';
    public $RapidAPIkey = "7b49280972mshd98e8d8eaaf647bp16bffejsn7d48cb949ce7";
    public $headers;
    public $headersVIN;
    public $client;
    public $URL = 'https://telesign-telesign-send-sms-verification-code-v1.p.rapidapi.com';
    public $URLVIN = 'https://vindecoder.p.rapidapi.com';
    public $RapidAPIhostVin = 'vindecoder.p.rapidapi.com';


    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client();
        $this->headers = [
            'X-RapidAPI-Hos' => $this->RapidAPIhost,
            'X-RapidAPI-Key'  => $this->RapidAPIkey,
        ];

        $this->headersVIN = [
            'X-RapidAPI-Host: '.$this->RapidAPIhostVin,
            'X-RapidAPI-Key: '.$this->RapidAPIkey,
        ];
    }

    public function ValidateRegisterPhoneNumber($phone,$code){
        
        $url = $this->URL.'/verifyCode='.$code.'&phoneNumber='.$phone;
        $response = $this->client->request('POST', 
            $url, [
            'headers' => [
                $this->headers
            ],
        ]);
       
        echo $response->getBody();
    }

    public function ValidateVIN_SalvageCheck($vin){
        //examplevin = 4T4BF1FKXER340134
        $url = $this->URLVIN.'/salvage_check?vin='.$vin;

        $curl = curl_init();
        
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => $this->headersVIN,
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return [
                'success' => 200,
                'data'    => json_decode($response,true),
            ];
        }
      
    }

    public function ValidateDecodeVIN($vin){
        //examplevin = 4T4BF1FKXER340134
        $url = $this->URLVIN.'/decode_vin?vin='.$vin;

        $curl = curl_init();
        
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => $this->headersVIN,
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return [
                'success' => 200,
                'data'    => json_decode($response,true),
            ];
        }
        
    }


}
